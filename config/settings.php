<?php

return [
    'super_admin' => 1,
    'admin' => 2,
    'user' => 0,
    'user_unactive' => 3,
    'user' => 0,
    'done' => 1,
    'error' => 0,
    'upload_path' => public_path() . '/assets/uploads/',
    'avatar_default_path' => 'https://image.flaticon.com/icons/png/128/23/23228.png',
];
